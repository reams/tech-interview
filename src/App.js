import './App.css';
import { AssetsContextProvider } from './Components/Context/AssetContext';
import AppRouter from './Components/Routes/Routes';

const App = () => (
  <div className="App">
    <AssetsContextProvider>
      <AppRouter />
    </AssetsContextProvider>
  </div>
);

export default App;

