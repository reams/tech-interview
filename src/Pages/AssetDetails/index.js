import { Image, Statistic, Tag, Divider, Col, Row } from 'antd';
import { useParams } from 'react-router-dom'
import { AssetContext } from '../../Components/Context/AssetContext';
import { useContext } from 'react';
import UserLayout from '../../Components/Layouts/userLayout';

const getAsset = (assets, id) => assets.find((asset => asset.id === id))

const AssetDetails = () => {

  const { id } = useParams();
  const { assets } = useContext(AssetContext)

  const asset = getAsset(assets, id)


  return (<UserLayout
    title="Asset Details"
    subTitle={asset.name}
    backButton>
    <Row>
      <Col span={12} >
        <Statistic title="Name" value={asset.name} />
        <Divider />
        <Image
          src={`/assets/${asset.id}.jpeg`}
        />
      </Col>
      <Col span={12} >
        <Statistic title="Location" value={asset.facility} />
        <Divider />
        <Statistic title="Installation Date" value={asset.installDate} />
        <Divider />
        <Statistic title="Condition" value={asset.condition} formatter={(cond) => (<Tag color="red">{cond}</Tag>)} />
      </Col>
    </Row>
  </UserLayout>
  )
}

export default AssetDetails;

