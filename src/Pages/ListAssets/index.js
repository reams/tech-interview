import { Descriptions, Table, Tag, Button } from 'antd';
import UserLayout from '../../Components/Layouts/userLayout';
import AllAssetTable from './AllAssetTable'
import { AssetContext } from '../../Components/Context/AssetContext';
import { useContext } from 'react';

const deleteAsset = (setAssets, assets) => {
  setAssets(assets.filter(asset => asset.id !== '1'))
}

const ListAssets = () => {

  const { assets, setAssets } = useContext(AssetContext)

  return (

    <UserLayout
      title="List Assets"
      buttons={
        [(<Button onClick={() => deleteAsset(setAssets, assets)}>
          Delete Asset Air Conditioning Unit
        </Button>)]
      }
    >
      <AllAssetTable assets={assets} />
    </UserLayout>
  )
};

export default ListAssets;

