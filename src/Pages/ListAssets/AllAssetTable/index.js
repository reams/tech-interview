import { Table, Tag, Space } from "antd";
import { Link } from "react-router-dom";
import Condition from "../../../Components/Core/Condition";

const columns = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
    render: (text) => <a>{text}</a>,
  },
  {
    title: "Location",
    dataIndex: "facility",
    key: "facility",
  },
  {
    title: "Installation Date",
    dataIndex: "installDate",
    key: "installDate",
  },
  {
    title: "Condition",
    key: "condition",
    dataIndex: "condition",
    render: (condition) => <Condition condition={condition} />,
  },
  {
    title: "Action",
    key: "action",
    render: (text, record) => (
      <Space size="middle">
        <Link to={`${record.id}`}>
          <a>View</a>
        </Link>
        <a>Duplicate</a>
        <a>Remove</a>
      </Space>
    ),
  },
];

const AllAssetTable = ({ assets }) => (
  <Table columns={columns} dataSource={assets} />
);

export default AllAssetTable;
