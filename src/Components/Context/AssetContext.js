import { createContext, useState } from "react";
import data from "./MockData";

export const AssetType = {
    HVAC: 'hvac',
    SANITARY: 'sanitary',
    ELECTRICAL: 'electrical'
}

const LifecycleData = {
    [`${AssetType.SANITARY}`]: {
        replacementCost: 200,
        tasks: [{
            title: 'Check Tubes',
            frequency: '1MONTH',
            cost: 150,
            time: 60
        },
        {
            title: 'Replace handle',
            frequency: '1YEAR',
            cost: 50,
            time: 45
        }]
    },
    [`${AssetType.HVAC}`]: {
        replacementCost: 500,
        tasks: [{
            title: 'Check Pressure',
            frequency: '1MONTH',
            cost: 45,
            time: 20
        },
        {
            title: 'Replace Fluid',
            frequency: '1YEAR',
            cost: 200,
            time: 60
        },
        {
            title: 'Statutory Maintenance',
            frequency: '1MONTH',
            cost: 10,
            time: 30
        }]
        
    },
    [`${AssetType.ELECTRICAL}`]: {
        replacementCost: 1500,
        tasks: [{
            title: 'Ensure Wiring Is Safe',
            frequency: '1MONTH',
            cost: 40,
            time: 15
        },
        {
            title: 'Re-wire unit',
            frequency: '1YEAR',
            cost: 500,
            time: 60
        }]
        
    }
}

export const AssetContext = createContext({ assets: data, LifecycleData, setAssets: () => { }, deleteAsset: () => { } });

export const AssetsContextProvider = (props) => {

    const setAssets = (assets) => {
        setState({ ...state, assets: assets })
    }

    const initState = {
        assets: data,
        setAssets: setAssets,
        LifecycleData
    }

    const [state, setState] = useState(initState)

    return (
        <AssetContext.Provider value={state}>
            {props.children}
        </AssetContext.Provider>
    )
}
