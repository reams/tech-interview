// import { AssetType } from "./AssetContext";
/**
 * THIS FILE IS A MOCK BACKEND
 * PLEASE DO NOT AMEND
 */

 export const AssetType = {
    HVAC: 'hvac',
    SANITARY: 'sanitary',
    ELECTRICAL: 'electrical'
}
const data = [
    {
        id: '1',
        quantity: 5,
        name: 'Air Conditioning Unit',
        installDate: '10/12/2017',
        facility: 'Aztec Centre, Bristol',
        lifespan: 2,
        type: AssetType.HVAC,
        condition: 'good',
    },
    {
        id: '2',
        quantity: 7,
        name: 'Electrical Control Panel',
        installDate: '03/09/2019',
        facility: 'Aztec Centre, Bristol',
        lifespan: 5,
        type: AssetType.ELECTRICAL,
        condition: 'poOr',
    },
    {
        id: '3',
        name: 'CO2 Monitor',
        quantity: 1,
        installDate: '06/10/2013',
        facility: 'Aztec Centre, Bristol',
        lifespan: 10,
        type: AssetType.SANITARY,
        condition: 'poor',
    },
    {
        id: '4',
        name: 'Toilet',
        quantity: 3,
        installDate: '09/02/2011',
        facility: 'Aztec Centre, Bristol',
        lifespan: 40,
        type: AssetType.SANITARY,
        condition: 'replace',
    },
    {
        id: '5',
        name: 'Solar Panels',
        quantity: 3,
        installDate: '09/02/2021',
        facility: 'Aztec Centre, Bristol',
        lifespan: 25,
        type: AssetType.ELECTRICAL,
        condition: 'good',
    },
];

export default data