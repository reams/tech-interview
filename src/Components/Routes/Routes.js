import { Routes, Route } from "react-router-dom";
import ListAssets from "../../Pages/ListAssets";
import AssetDetails from "../../Pages/AssetDetails";

const AppRouter = () => {
    return (
        <Routes>
            <Route path="/" element={<ListAssets />} />
            <Route path="/:id" element={<AssetDetails />} />
        </Routes>
    )
}

export default AppRouter