import { PageHeader, Row, Col } from 'antd';

const UserLayout = ({ backButton, title, subTitle, buttons, children }) => (
    <Row>
        <Col span={12} offset={6}>
            <PageHeader
                ghost={false}
                onBack={backButton ? () => window.history.back() : undefined}
                title={title}
                subTitle={subTitle}
                extra={buttons}
            >
                {children}
            </PageHeader>
        </Col>
    </Row>
);

export default UserLayout;

