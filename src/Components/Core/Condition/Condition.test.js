/*
  use: npm run test
*/

import { render, screen } from "@testing-library/react";
import Condition from ".";

describe("The condition component should render values to screen correctly", () => {

  test(`should render the condition text to the screen`, () => {
    // Arainge
    const condition = "theCondition";

    // Act
    render(<Condition condition={condition} />);
    const linkElement = screen.getByText(condition);

    // Assert
    expect(linkElement).toBeInTheDocument();
  });

  test("should render the text in the correct format", () => {
    // Arainge
    const condition = "poOr";
    const comparison = "Poor"

    // Act
    render(<Condition condition={condition} />);
    const linkElement = screen.getByText(comparison);

    // Assert
    expect(linkElement).toBeInTheDocument();
  });
});

describe("Condition Background Color is conditional on Condition", () => {
  test("colour should be volcano when the condition is Poor", () => {});

  test("colour should be geekblue when the condition is Replace", () => {});

  test("colour should be green when the condition is Good", () => {});

  test("colour should be white when the condition is not Poor, Replace or Good", () => {});
});
