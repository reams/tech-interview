import { Tag } from "antd";

export const conditionBackground = (condition) => {
  let colour = condition.length > 5 ? "geekblue" : "green";
  if (condition === "poor") {
    colour = "volcano";
  }
  return colour;
};

const Condition = ({ condition }) => {
  const colour = conditionBackground(condition);
  return (
    <Tag color={colour} key={condition}>
      {condition}
    </Tag>
  );
};

export default Condition;
